//! SSH Types
mod kex;
mod shk;

pub use types::kex::{KeyExchange, KeyExchangeAlgorithm};
pub use types::shk::{ServerHostKey, ServerHostKeyAlgorithm, ServerHostKeyType};

use GoopyResult;
use error::GoopyErr;
use message::kexinit;
use regex::Regex;
use std::convert::TryFrom;
use std::fmt;

lazy_static! {
    /// Version Exchange Protocol Regular Expression
    pub static ref VER_EXCH_PROTO: Regex =
        Regex::new(r#"^SSH-2.0-([\x21-\x2C\x2E-\x7E]+)([ ][\x21-\x2C\x2E-\x7E]+)?\r\n$"#).unwrap();
}

/// SSH ID String struct
pub struct SshId {
    id: String,
}

impl SshId {
    /// Create a new SshId given the name, version, and optional comment.
    ///
    /// This will form a string of the format `SSH-2.0-<name>_<version> <comment>\r\n`
    pub fn new(name: &str, version: &str, comment: Option<&str>) -> GoopyResult<SshId> {
        let mut id = String::from("SSH-2.0-");
        id.push_str(name);
        id.push('_');
        id.push_str(version);

        if let Some(com) = comment {
            id.push(' ');
            id.push_str(com);
        }

        id.push_str("\r\n");

        if id.len() < 256 {
            Ok(SshId { id: id })
        } else {
            Err(GoopyErr::InvalidSshId)
        }
    }

    /// The string representation of the SshId.
    pub fn id(&self) -> &str {
        &self.id
    }
}

/// Version exchange struct
#[derive(Default)]
pub struct VersionExchange {
    /// Server Version
    server_version: String,
    /// Client Version
    client_version: String,
}

impl VersionExchange {
    /// Get the client version
    pub fn client_version(&self) -> &String {
        &self.client_version
    }

    /// Get the server version.
    pub fn server_version(&self) -> &String {
        &self.server_version
    }

    /// Set the client version.
    pub fn set_client_version(&mut self, client_version: String) -> &mut VersionExchange {
        self.client_version = client_version;
        self
    }

    /// Set the server version.
    pub fn set_server_version(&mut self, server_version: String) -> &mut VersionExchange {
        self.server_version = server_version;
        self
    }
}

impl fmt::Display for VersionExchange {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Server: {}, Client: {}",
               self.server_version.trim(),
               self.client_version.trim())
    }
}

#[derive(Copy, Clone, Debug)]
/// After the Key Exchange, this represents the decided upon algorithms to use for this session.
pub struct ConnProps {
    /// The key exchange algorithm used by the session.
    kex_algorithm: KeyExchangeAlgorithm,
    /// The server host key algorithm used by the session.
    server_host_key_algorithm: ServerHostKeyAlgorithm,
    /// The encryption algorithm used by a connection for client-to-server communication.
    encryption_algo_c2s: EncryptionAlgorithm,
    /// The encryption algorithm used by a connection for server-to-client communication.
    encryption_algo_s2c: EncryptionAlgorithm,
    /// The cipher block size used by the client-to-server encryption algorithm.
    cipher_block_size_c2s: CipherBlockSize,
    /// The cipher block size used by the server-to-client encryption algorithm.
    cipher_block_size_s2c: CipherBlockSize,
    /// The MAC algorithm used by a connection for client-to-server communication.
    mac_algo_c2s: MacAlgorithm,
    /// The MAC algorithm used by a connection for server-to-client communication.
    mac_algo_s2c: MacAlgorithm,
    /// The compression algorithm used by a connection for client-to-server communication.
    comp_algo_c2s: CompressionAlgorithm,
    /// The compression algorithm used by a connection for server-to-client communication.
    comp_algo_s2c: CompressionAlgorithm,
}

impl ConnProps {
    /// Create a new `ConnProps` struct given the clien and server key exchange messages.
    /// See https://tools.ietf.org/html/rfc4253#section-7.1 for the rules regarding choosing the
    /// proper values.
    pub fn new(client_kex: kexinit::Message,
               server_kex: kexinit::Message)
               -> GoopyResult<ConnProps> {
        let server_shk_algos: Vec<ServerHostKey> =
            server_kex.shk_algos().into_iter().filter_map(|x| ServerHostKey::new(x).ok()).collect();
        let kex_algo = if client_kex.kex_algos()[0] == server_kex.kex_algos()[0] {
            try!(KeyExchangeAlgorithm::try_from(&client_kex.kex_algos()[0]))
        } else {
            let mut found_kex_algo: Option<KeyExchangeAlgorithm> = None;
            for kex_algo_str in client_kex.kex_algos() {
                let kex_algo = try!(KeyExchangeAlgorithm::try_from(&kex_algo_str));
                if let Ok(res) = find_kex_algo(&kex_algo,
                                               &server_kex.kex_algos()[..],
                                               &server_shk_algos) {
                    if res {
                        found_kex_algo = Some(kex_algo);
                        break;
                    }
                }
            }
            if let Some(kex_algo) = found_kex_algo {
                kex_algo
            } else {
                return Err(GoopyErr::InvalidKexAlgorithm("".to_string()));
            }
        };

        Ok(ConnProps {
            kex_algorithm: kex_algo,
            server_host_key_algorithm: ServerHostKeyAlgorithm::Ed25519,
            encryption_algo_c2s: EncryptionAlgorithm::Aes256Cbc,
            encryption_algo_s2c: EncryptionAlgorithm::Aes256Cbc,
            cipher_block_size_c2s: CipherBlockSize::Eight,
            cipher_block_size_s2c: CipherBlockSize::Eight,
            mac_algo_c2s: MacAlgorithm::Blake2b512,
            mac_algo_s2c: MacAlgorithm::Blake2b512,
            comp_algo_c2s: CompressionAlgorithm::NoComp,
            comp_algo_s2c: CompressionAlgorithm::NoComp,
        })
    }
}

fn find_kex_algo(_kex_algo: &KeyExchangeAlgorithm,
                 _server_algos: &[String],
                 _server_shk_algos: &[ServerHostKey])
                 -> GoopyResult<bool> {
    Ok(true)
}

#[derive(Copy, Clone, Debug, PartialEq)]
/// Cipher Block Size in bytes.  Currently 8 bytes (64 bits) or 16 bytes (128 bits).
pub enum CipherBlockSize {
    /// 8 byte block
    Eight,
    /// 16 byte block
    Sixteen,
}

use self::CipherBlockSize::{Eight, Sixteen};

impl fmt::Display for CipherBlockSize {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let val = match *self {
            Eight => 8,
            Sixteen => 16,
        };
        write!(f, "{}", val)
    }
}

impl From<u8> for CipherBlockSize {
    fn from(v: u8) -> CipherBlockSize {
        match v {
            16 => Sixteen,
            _ => Eight,
        }
    }
}

impl From<CipherBlockSize> for u8 {
    fn from(cbs: CipherBlockSize) -> u8 {
        match cbs {
            Eight => 8,
            Sixteen => 16,
        }
    }
}

#[test]
fn cbs() {
    assert!(format!("{}", Eight) == "8");
    assert!(format!("{}", Sixteen) == "16");
    assert!(CipherBlockSize::from(0) == Eight);
    assert!(CipherBlockSize::from(8) == Eight);
    assert!(CipherBlockSize::from(16) == Sixteen);
    assert!(u8::from(Eight) == 8);
    assert!(u8::from(Sixteen) == 16);
}

#[derive(Copy, Clone, Debug, PartialEq)]
/// Allowed Encryption algorithms.
pub enum EncryptionAlgorithm {
    /// AES with 128-bit key in cipher block chain mode.
    Aes128Cbc,
    /// AES with 192-bit key in cipher block chain mode.
    Aes192Cbc,
    /// AES with 256-bit key in cipher block chain mode.
    Aes256Cbc,
    /// Arcfour
    Arcfour,
    /// Blowfish in cipher block chain mode.
    BlowfishCbc,
    /// CAST-128 in cipher block chain mode.
    Cast128Cbc,
    /// Idea in cipher block chain mode.
    IdeaCbc,
    /// Serpent with 128-bit key in cipher block chain mode.
    Serpent128Cbc,
    /// Serpent with 192-bit key in cipher block chain mode.
    Serpent192Cbc,
    /// Serpent with 256-bit key in cipher block chain mode.
    Serpent256Cbc,
    /// Triple-DES in cipher block chain mode.
    TripleDesCbc,
    /// Twofish with 128-bit key in cipher block chain mode.
    Twofish128Cbc,
    /// Twofish with 192-bit key in cipher block chain mode.
    Twofish192Cbc,
    /// Twofish with 256-bit key in cipher block chain mode.
    Twofish256Cbc,
    /// No encryption (not recommended!)
    NoEnc,
}

use self::EncryptionAlgorithm::{Aes128Cbc, Aes192Cbc, Aes256Cbc, Arcfour, BlowfishCbc, Cast128Cbc,
                                IdeaCbc, NoEnc, Serpent128Cbc, Serpent192Cbc, Serpent256Cbc,
                                TripleDesCbc, Twofish128Cbc, Twofish192Cbc, Twofish256Cbc};

impl fmt::Display for EncryptionAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let enc_str = match *self {
            Aes128Cbc => "aes128-cbc",
            Aes192Cbc => "aes192-cbc",
            Aes256Cbc => "aes256-cbc",
            Arcfour => "arcfour",
            BlowfishCbc => "blowfish-cbc",
            Cast128Cbc => "cast128-cbc",
            IdeaCbc => "idea-cbc",
            Serpent128Cbc => "serpent128-cbc",
            Serpent192Cbc => "serpent192-cbc",
            Serpent256Cbc => "serpent256-cbc",
            TripleDesCbc => "3des-cbc",
            Twofish256Cbc => "twofish256-cbc",
            Twofish192Cbc => "twofish192-cbc",
            Twofish128Cbc => "twofish128-cbc",
            NoEnc => "none",
        };
        write!(f, "{}", enc_str)
    }
}

impl<'a> TryFrom<&'a str> for EncryptionAlgorithm {
    type Err = GoopyErr;

    fn try_from(enc_algo: &'a str) -> Result<EncryptionAlgorithm, GoopyErr> {
        if enc_algo == "aes128-cbc" {
            Ok(Aes128Cbc)
        } else if enc_algo == "aes192-cbc" {
            Ok(Aes192Cbc)
        } else if enc_algo == "aes256-cbc" {
            Ok(Aes256Cbc)
        } else if enc_algo == "arcfour" {
            Ok(Arcfour)
        } else if enc_algo == "blowfish-cbc" {
            Ok(BlowfishCbc)
        } else if enc_algo == "cast128-cbc" {
            Ok(Cast128Cbc)
        } else if enc_algo == "idea-cbc" {
            Ok(IdeaCbc)
        } else if enc_algo == "serpent128-cbc" {
            Ok(Serpent128Cbc)
        } else if enc_algo == "serpent192-cbc" {
            Ok(Serpent192Cbc)
        } else if enc_algo == "serpent256-cbc" {
            Ok(Serpent256Cbc)
        } else if enc_algo == "3des-cbc" {
            Ok(TripleDesCbc)
        } else if enc_algo == "twofish128-cbc" {
            Ok(Twofish128Cbc)
        } else if enc_algo == "twofish192-cbc" {
            Ok(Twofish192Cbc)
        } else if enc_algo == "twofish256-cbc" {
            Ok(Twofish256Cbc)
        } else if enc_algo == "none" {
            Ok(NoEnc)
        } else {
            Err(GoopyErr::InvalidEncAlgorithm(enc_algo.to_string()))
        }
    }
}

#[test]
fn enc_algos() {
    assert!(format!("{}", Aes128Cbc) == "aes128-cbc");
    assert!(format!("{}", Aes192Cbc) == "aes192-cbc");
    assert!(format!("{}", Aes256Cbc) == "aes256-cbc");
    assert!(format!("{}", Arcfour) == "arcfour");
    assert!(format!("{}", BlowfishCbc) == "blowfish-cbc");
    assert!(format!("{}", Cast128Cbc) == "cast128-cbc");
    assert!(format!("{}", IdeaCbc) == "idea-cbc");
    assert!(format!("{}", Serpent128Cbc) == "serpent128-cbc");
    assert!(format!("{}", Serpent192Cbc) == "serpent192-cbc");
    assert!(format!("{}", Serpent256Cbc) == "serpent256-cbc");
    assert!(format!("{}", TripleDesCbc) == "3des-cbc");
    assert!(format!("{}", Twofish128Cbc) == "twofish128-cbc");
    assert!(format!("{}", Twofish192Cbc) == "twofish192-cbc");
    assert!(format!("{}", Twofish256Cbc) == "twofish256-cbc");
    assert!(format!("{}", NoEnc) == "none");
}

#[test]
fn try_from_enc_algos() {
    assert!(Aes128Cbc == EncryptionAlgorithm::try_from("aes128-cbc").expect("fail"));
    assert!(Aes192Cbc == EncryptionAlgorithm::try_from("aes192-cbc").expect("fail"));
    assert!(Aes256Cbc == EncryptionAlgorithm::try_from("aes256-cbc").expect("fail"));
    assert!(Arcfour == EncryptionAlgorithm::try_from("arcfour").expect("fail"));
    assert!(BlowfishCbc == EncryptionAlgorithm::try_from("blowfish-cbc").expect("fail"));
    assert!(Cast128Cbc == EncryptionAlgorithm::try_from("cast128-cbc").expect("fail"));
    assert!(IdeaCbc == EncryptionAlgorithm::try_from("idea-cbc").expect("fail"));
    assert!(Serpent128Cbc == EncryptionAlgorithm::try_from("serpent128-cbc").expect("fail"));
    assert!(Serpent192Cbc == EncryptionAlgorithm::try_from("serpent192-cbc").expect("fail"));
    assert!(Serpent256Cbc == EncryptionAlgorithm::try_from("serpent256-cbc").expect("fail"));
    assert!(TripleDesCbc == EncryptionAlgorithm::try_from("3des-cbc").expect("fail"));
    assert!(Twofish128Cbc == EncryptionAlgorithm::try_from("twofish128-cbc").expect("fail"));
    assert!(Twofish192Cbc == EncryptionAlgorithm::try_from("twofish192-cbc").expect("fail"));
    assert!(Twofish256Cbc == EncryptionAlgorithm::try_from("twofish256-cbc").expect("fail"));
    assert!(NoEnc == EncryptionAlgorithm::try_from("none").expect("fail"));
    assert!(EncryptionAlgorithm::try_from("bad_algo").is_err());
}

#[derive(Copy, Clone, Debug, PartialEq)]
/// Allowed MAC algorithms
pub enum MacAlgorithm {
    /// Blake2 hash
    Blake2b512,
    /// HMAC with SHA-256 hash
    HmacSha256,
    /// No MAC defined (not recommended!)
    NoMac,
}

use self::MacAlgorithm::{Blake2b512, HmacSha256, NoMac};

impl fmt::Display for MacAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mac_str = match *self {
            Blake2b512 => "blake2b-512",
            HmacSha256 => "hmac-sha256",
            NoMac => "none",
        };
        write!(f, "{}", mac_str)
    }
}

impl<'a> TryFrom<&'a str> for MacAlgorithm {
    type Err = GoopyErr;

    fn try_from(mac_algo: &'a str) -> Result<MacAlgorithm, GoopyErr> {
        if mac_algo == "blake2b-512" {
            Ok(Blake2b512)
        } else if mac_algo == "hmac-sha256" {
            Ok(HmacSha256)
        } else if mac_algo == "none" {
            Ok(NoMac)
        } else {
            Err(GoopyErr::InvalidMacAlgorithm(mac_algo.to_string()))
        }
    }
}

#[test]
fn mac_algos() {
    assert!(format!("{}", Blake2b512) == "blake2b-512");
    assert!(format!("{}", HmacSha256) == "hmac-sha256");
    assert!(format!("{}", NoMac) == "none");
}

#[test]
fn try_from_mac_algos() {
    assert!(Blake2b512 == MacAlgorithm::try_from("blake2b-512").expect("fail"));
    assert!(HmacSha256 == MacAlgorithm::try_from("hmac-sha256").expect("fail"));
    assert!(NoMac == MacAlgorithm::try_from("none").expect("fail"));
    assert!(MacAlgorithm::try_from("bad_algo").is_err());
}

#[derive(Copy, Clone, Debug, PartialEq)]
/// Allowed compression algorithms
pub enum CompressionAlgorithm {
    /// xz compression
    Xz,
    /// zlib compression
    Zlib,
    /// No compression defined.
    NoComp,
}

use self::CompressionAlgorithm::{NoComp, Xz, Zlib};

impl fmt::Display for CompressionAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let comp_str = match *self {
            Xz => "xz",
            Zlib => "zlib",
            NoComp => "none",
        };
        write!(f, "{}", comp_str)
    }
}

impl<'a> TryFrom<&'a str> for CompressionAlgorithm {
    type Err = GoopyErr;

    fn try_from(comp_algo: &'a str) -> Result<CompressionAlgorithm, GoopyErr> {
        if comp_algo == "xz" {
            Ok(Xz)
        } else if comp_algo == "zlib" {
            Ok(Zlib)
        } else if comp_algo == "none" {
            Ok(NoComp)
        } else {
            Err(GoopyErr::InvalidCompAlgorithm(comp_algo.to_string()))
        }
    }
}

#[test]
fn comp_algos() {
    assert!(format!("{}", Xz) == "xz");
    assert!(format!("{}", Zlib) == "zlib");
    assert!(format!("{}", NoComp) == "none");
}

#[test]
fn try_from_comp_algos() {
    assert!(Xz == CompressionAlgorithm::try_from("xz").expect("fail"));
    assert!(Zlib == CompressionAlgorithm::try_from("zlib").expect("fail"));
    assert!(NoComp == CompressionAlgorithm::try_from("none").expect("fail"));
    assert!(CompressionAlgorithm::try_from("bad_algo").is_err());
}
