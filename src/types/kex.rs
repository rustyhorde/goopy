use GoopyResult;
use error::GoopyErr;
use std::convert::TryFrom;
use std::fmt;
use types::ServerHostKeyType;

/// A `KeyExchange` defined by a [`KeyExchangeAlgorithm`][kexa] and the associated optional
/// [`ServerHostKeyType`][shkt] if required by the algorithm.
/// [kexa]: enum.KeyExchangeAlgorithm.html
/// [shkt]: enum.ServerHostKeyType.html
pub struct KeyExchange {
    kex_algorithm: KeyExchangeAlgorithm,
    req_shk_type: Option<ServerHostKeyType>,
}

impl fmt::Display for KeyExchange {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.req_shk_type {
            Some(rst) => write!(f, "{} ({})", self.kex_algorithm, rst),
            None => write!(f, "{}", self.kex_algorithm),
        }
    }
}

impl KeyExchange {
    /// Create a `KeyExchange` struct from the given algorithm
    pub fn new(algo: &str) -> GoopyResult<KeyExchange> {
        if let Ok(kex_algo) = KeyExchangeAlgorithm::try_from(algo) {
            // TODO: Determine the required server host key types and assign here.
            let req_shk_type = match kex_algo {
                _ => None,
            };
            Ok(KeyExchange {
                kex_algorithm: kex_algo,
                req_shk_type: req_shk_type,
            })
        } else {
            Err(GoopyErr::InvalidKexAlgorithm(algo.to_string()))
        }
    }
}

#[test]
fn kex() {
    match KeyExchange::new("curve25519-blake2") {
        Ok(kex) => {
            assert!(kex.kex_algorithm == KeyExchangeAlgorithm::Curve25519Blake2);
            assert!(kex.req_shk_type == None);
        }
        Err(_) => panic!("Invalid KeyExchange"),
    }

    match KeyExchange::new("curve25519-sha256@libssh.org") {
        Ok(kex) => {
            assert!(kex.kex_algorithm == KeyExchangeAlgorithm::Curve25519Sha256);
            assert!(kex.req_shk_type == None);
        }
        Err(_) => panic!("Invalid KeyExchange"),
    }

    match KeyExchange::new("diffie-hellman-group-exchange-sha256") {
        Ok(kex) => {
            assert!(kex.kex_algorithm == KeyExchangeAlgorithm::ExchangeSha256);
            assert!(kex.req_shk_type == None);
        }
        Err(_) => panic!("Invalid KeyExchange"),
    }

    assert!(KeyExchange::new("bad-algo").is_err());
}

#[derive(Copy, Clone, Debug, PartialEq, RustcDecodable)]
/// Allowed Key Exchange algorithms.
pub enum KeyExchangeAlgorithm {
    /// Curve22519 with Blake2 hash.
    Curve25519Blake2,
    /// Curve22519 with SHA-256 hash.
    Curve25519Sha256,
    /// Diffie-Hellman group exchange with SHA-256 hash.
    ExchangeSha256,
}

use self::KeyExchangeAlgorithm::{Curve25519Blake2, Curve25519Sha256, ExchangeSha256};

impl fmt::Display for KeyExchangeAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let kex_str = match *self {
            Curve25519Blake2 => "curve25519-blake2",
            Curve25519Sha256 => "curve25519-sha256@libssh.org",
            ExchangeSha256 => "diffie-hellman-group-exchange-sha256",
        };
        write!(f, "{}", kex_str)
    }
}

impl<'a> TryFrom<&'a str> for KeyExchangeAlgorithm {
    type Err = GoopyErr;

    fn try_from(kex_algo: &'a str) -> Result<KeyExchangeAlgorithm, GoopyErr> {
        if kex_algo == "curve25519-blake2" {
            Ok(Curve25519Blake2)
        } else if kex_algo == "curve25519-sha256@libssh.org" {
            Ok(Curve25519Sha256)
        } else if kex_algo == "diffie-hellman-group-exchange-sha256" {
            Ok(ExchangeSha256)
        } else {
            Err(GoopyErr::InvalidKexAlgorithm(kex_algo.to_string()))
        }
    }
}

#[test]
fn kex_algos() {
    assert!(format!("{}", Curve25519Blake2) == "curve25519-blake2");
    assert!(format!("{}", Curve25519Sha256) == "curve25519-sha256@libssh.org");
    assert!(format!("{}", ExchangeSha256) == "diffie-hellman-group-exchange-sha256");
}

#[test]
fn try_from_kex_algos() {
    assert!(Curve25519Blake2 == KeyExchangeAlgorithm::try_from("curve25519-blake2").expect("fail"));
    assert!(Curve25519Sha256 ==
            KeyExchangeAlgorithm::try_from("curve25519-sha256@libssh.org").expect("fail"));
    assert!(ExchangeSha256 ==
            KeyExchangeAlgorithm::try_from("diffie-hellman-group-exchange-sha256").expect("fail"));
    assert!(KeyExchangeAlgorithm::try_from("bad_algo").is_err());
}
