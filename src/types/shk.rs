use GoopyResult;
use error::GoopyErr;
use std::convert::TryFrom;
use std::fmt;

#[derive(Copy, Clone, Debug, PartialEq)]
/// Server Host Key Types
pub enum ServerHostKeyType {
    /// The server host key algorithm supports signing only.
    Sign,
    /// The server host key algorithm supports encryption only.
    Encrypt,
    /// The server host key algorithm supports both signing and encryption.
    SignEncrypt,
}

use self::ServerHostKeyType::{Encrypt, Sign, SignEncrypt};

impl fmt::Display for ServerHostKeyType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let shkt_str = match *self {
            Sign => "sign",
            Encrypt => "encrypt",
            SignEncrypt => "sign_encrypt",
        };
        write!(f, "{}", shkt_str)
    }
}

impl<'a> TryFrom<&'a str> for ServerHostKeyType {
    type Err = GoopyErr;

    fn try_from(shkt: &'a str) -> Result<ServerHostKeyType, GoopyErr> {
        if shkt == "sign" {
            Ok(Sign)
        } else if shkt == "encrypt" {
            Ok(Encrypt)
        } else if shkt == "sign_encrypt" {
            Ok(SignEncrypt)
        } else {
            Err(GoopyErr::InvalidShkType(shkt.to_string()))
        }
    }
}

#[test]
fn shk_types() {
    assert!(format!("{}", Sign) == "sign");
    assert!(format!("{}", Encrypt) == "encrypt");
    assert!(format!("{}", SignEncrypt) == "sign_encrypt");
}

#[test]
fn try_from_shk_types() {
    assert!(Sign == ServerHostKeyType::try_from("sign").expect("fail"));
    assert!(Encrypt == ServerHostKeyType::try_from("encrypt").expect("fail"));
    assert!(SignEncrypt == ServerHostKeyType::try_from("sign_encrypt").expect("fail"));
    assert!(ServerHostKeyType::try_from("bad_type").is_err());
}

#[derive(Copy, Clone, Debug, PartialEq)]
/// Allowed Server Host Key algorithms.
pub enum ServerHostKeyAlgorithm {
    /// ed25519 based host key
    Ed25519,
    /// RSA base host key
    Rsa,
}

use self::ServerHostKeyAlgorithm::{Ed25519, Rsa};

impl fmt::Display for ServerHostKeyAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let shk_str = match *self {
            Ed25519 => "ssh-ed25519",
            Rsa => "ssh-rsa",
        };
        write!(f, "{}", shk_str)
    }
}

impl<'a> TryFrom<&'a str> for ServerHostKeyAlgorithm {
    type Err = GoopyErr;

    fn try_from(shk_algo: &'a str) -> Result<ServerHostKeyAlgorithm, GoopyErr> {
        if shk_algo == "ssh-ed25519" {
            Ok(Ed25519)
        } else if shk_algo == "ssh-rsa" {
            Ok(Rsa)
        } else {
            Err(GoopyErr::InvalidShkAlgorithm(shk_algo.to_string()))
        }
    }
}

#[test]
fn shk_algos() {
    assert!(format!("{}", Ed25519) == "ssh-ed25519");
    assert!(format!("{}", Rsa) == "ssh-rsa");
}

#[test]
fn try_from_shk_algos() {
    assert!(Ed25519 == ServerHostKeyAlgorithm::try_from("ssh-ed25519").expect("fail"));
    assert!(Rsa == ServerHostKeyAlgorithm::try_from("ssh-rsa").expect("fail"));
    assert!(ServerHostKeyAlgorithm::try_from("bad_algo").is_err());
}

#[derive(Copy, Clone, Debug, PartialEq)]
/// A `ServerHostKey` defined by a [`ServerHostKeyAlgorithm`][shka] and the associated
/// [`ServerHostKeyType`][shkt].
/// [shka]: enum.ServerHostKeyAlgorithm.html
/// [shkt]: enum.ServerHostKeyType.html
pub struct ServerHostKey {
    shk_algorithm: ServerHostKeyAlgorithm,
    shk_type: ServerHostKeyType,
}

impl fmt::Display for ServerHostKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} ({})", self.shk_algorithm, self.shk_type)
    }
}

impl ServerHostKey {
    /// Create a `ServerHostKey` struct from the given algorithm and type.l
    pub fn new(algo: &str) -> GoopyResult<ServerHostKey> {
        if let Ok(shk_algo) = ServerHostKeyAlgorithm::try_from(algo) {
            let shk_type = match shk_algo {
                ServerHostKeyAlgorithm::Ed25519 |
                ServerHostKeyAlgorithm::Rsa => ServerHostKeyType::Sign,
            };
            Ok(ServerHostKey {
                shk_algorithm: shk_algo,
                shk_type: shk_type,
            })
        } else {
            Err(GoopyErr::InvalidShkAlgorithm(algo.to_string()))
        }
    }
}

#[test]
fn shk() {
    match ServerHostKey::new("ssh-rsa") {
        Ok(shk) => {
            assert!(shk.shk_algorithm == ServerHostKeyAlgorithm::Rsa);
            assert!(shk.shk_type == ServerHostKeyType::Sign);
        }
        Err(_) => panic!("Invalid ServerHostKey"),
    }

    match ServerHostKey::new("ssh-ed25519") {
        Ok(shk) => {
            assert!(shk.shk_algorithm == ServerHostKeyAlgorithm::Ed25519);
            assert!(shk.shk_type == ServerHostKeyType::Sign);
        }
        Err(_) => panic!("Invalid ServerHostKey"),
    }

    assert!(ServerHostKey::new("ssh-invalid").is_err());
}
