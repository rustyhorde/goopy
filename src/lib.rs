// Copyright (c) 2016 goopy developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! goopy - SSH Library for goopd and gosh.
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![cfg_attr(feature="clippy", deny(clippy))]
#![deny(missing_docs)]
#![feature(try_from)]
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate rustc_serialize;

extern crate byteorder;
extern crate bytes;
extern crate libc;
extern crate rand;
extern crate regex;

use error::GoopyErr;

pub mod crypto;
pub mod error;
pub mod message;
pub mod packet;
pub mod types;
pub mod utils;

/// Result type for the goopy library API.
pub type GoopyResult<T> = Result<T, GoopyErr>;
