//! Runtime Errors

use std::error;
use std::fmt;
use std::io;

#[derive(Debug)]
/// Errors generated by the goopy library.
pub enum GoopyErr {
    /// Generated by try_from if the given compression algorithm is invalid.
    InvalidCompAlgorithm(String),
    /// Generated by try_from if the given encryption algorithm is invalid.
    InvalidEncAlgorithm(String),
    /// Generated by try_from if the given key exchange algorithm is invalid.
    InvalidKexAlgorithm(String),
    /// Generated by try_from if the given MAC algorithm is invalid.
    InvalidMacAlgorithm(String),
    /// Generated if a packet name list is invalid.
    InvalidNameList,
    /// Generated if the given packet has an invalid byte.
    InvalidPacketByte(&'static str),
    /// Generated by try_from if the given server host key algorithm is invalid.
    InvalidShkAlgorithm(String),
    /// Generated by try_from if the given server host key type is invalid.
    InvalidShkType(String),
    /// Generated if an SshId is longer than 256 bytes.
    InvalidSshId,
    /// `std::io::Error` wrapper
    Io(io::Error),
    /// Generated if the padding length cannot be determined.
    PaddingLength,
    /// Unknown ssh message
    UnknownMessage(u8),
}

impl fmt::Display for GoopyErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            GoopyErr::InvalidCompAlgorithm(ref s) => {
                write!(f, "Invalid compression algorithm name: {}", s)
            }
            GoopyErr::InvalidEncAlgorithm(ref s) => {
                write!(f, "Invalid encryption algorithm name: {}", s)
            }
            GoopyErr::InvalidKexAlgorithm(ref s) => {
                write!(f, "Invalid key exchange algorithm name: {}", s)
            }
            GoopyErr::InvalidMacAlgorithm(ref s) => write!(f, "Invalid MAC algorithm name: {}", s),
            GoopyErr::InvalidNameList => write!(f, "Invalid name-list"),
            GoopyErr::InvalidPacketByte(s) => write!(f, "Invalid packet byte: {}", s),
            GoopyErr::InvalidShkAlgorithm(ref s) => {
                write!(f, "Invalid server host key algorithm name: {}", s)
            }
            GoopyErr::InvalidShkType(ref s) => {
                write!(f, "Invalid server host key type name: {}", s)
            }
            GoopyErr::InvalidSshId => write!(f, "Invalid ssh id"),
            GoopyErr::Io(ref err) => err.fmt(f),
            GoopyErr::PaddingLength => write!(f, "Unable to determine padding length"),
            GoopyErr::UnknownMessage(ref m) => write!(f, "Unknown message: {}", m),
        }
    }
}

impl error::Error for GoopyErr {
    fn description(&self) -> &str {
        match *self {
            GoopyErr::InvalidCompAlgorithm(_) => "Invalid compression algorithm name",
            GoopyErr::InvalidEncAlgorithm(_) => "Invalid encryption algorithm name",
            GoopyErr::InvalidKexAlgorithm(_) => "Invalid key exchange algorithm name",
            GoopyErr::InvalidMacAlgorithm(_) => "Invalid MAC algorithm name",
            GoopyErr::InvalidNameList => "Invalid name-list",
            GoopyErr::InvalidPacketByte(_) => "Invalid packet byte",
            GoopyErr::InvalidShkAlgorithm(_) => "Invalid server host key algorithm name",
            GoopyErr::InvalidShkType(_) => "Invalid server host key type name",
            GoopyErr::InvalidSshId => "Invalid ssh id",
            GoopyErr::Io(ref err) => err.description(),
            GoopyErr::PaddingLength => "Unable to determine padding length",
            GoopyErr::UnknownMessage(_) => "Unknown message received",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            GoopyErr::InvalidCompAlgorithm(_) |
            GoopyErr::InvalidEncAlgorithm(_) |
            GoopyErr::InvalidKexAlgorithm(_) |
            GoopyErr::InvalidMacAlgorithm(_) |
            GoopyErr::InvalidNameList |
            GoopyErr::InvalidPacketByte(_) |
            GoopyErr::InvalidShkAlgorithm(_) |
            GoopyErr::InvalidShkType(_) |
            GoopyErr::InvalidSshId |
            GoopyErr::PaddingLength |
            GoopyErr::UnknownMessage(_) => None,
            GoopyErr::Io(ref err) => Some(err),
        }
    }
}

impl From<io::Error> for GoopyErr {
    fn from(err: io::Error) -> GoopyErr {
        GoopyErr::Io(err)
    }
}

/// goopy cryptograpy errors
#[derive(Debug)]
pub enum CryptoErr {
    /// Thrown if a byte array length is incorrect.
    ByteArrayLength(String),
    /// When comparing bytes in memory, the length must be equal.
    LengthNotEqual,
    /// Thrown if memory cannot be locked to avoid swapping to disk.
    MemLock(usize),
    /// Thrown on errors during memory protect operations.
    MemProtect,
    /// Thrown if a null pointer is detected.
    NullPtr,
}

impl error::Error for CryptoErr {
    fn description(&self) -> &str {
        match *self {
            CryptoErr::ByteArrayLength(ref s) => s,
            CryptoErr::LengthNotEqual => "the lengths must be equal when comparing bytes",
            CryptoErr::MemLock(_) => "unable to lock/unlock memory",
            CryptoErr::MemProtect => "unable to protect/unprotect memory",
            CryptoErr::NullPtr => "null pointer detected",
        }
    }
}

impl fmt::Display for CryptoErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            CryptoErr::ByteArrayLength(ref s) => write!(f, "{}", s),
            CryptoErr::LengthNotEqual => {
                write!(f, "the lengths must be equal when comparing bytes")
            }
            CryptoErr::MemLock(len) => write!(f, "unable to lock/unlock {} bytes of memory", len),
            CryptoErr::MemProtect => write!(f, "unable to protect/unprotect memory"),
            CryptoErr::NullPtr => write!(f, "null pointer"),
        }
    }
}
