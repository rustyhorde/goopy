//! Binary Packet Protocol - [RFC4253 Section 6][rfc4253]
//!
//! Each packet is in the following format:
//!
//! ```ignored
//!   uint32    packet_length
//!   byte      padding_length
//!   byte[n1]  payload; n1 = packet_length - padding_length - 1
//!   byte[n2]  random padding; n2 = padding_length
//!   byte[m]   mac (Message Authentication Code - MAC); m = mac_length
//! ```
//!
//! > `packet_length`
//! > > The length of the packet in bytes, not including `mac` or the
//! > > `packet_length` field itself.
//!
//! > `payload`
//! > > The useful contents of the packet.  If compression has been
//! > > negotiated, this field is compressed.  Initially, compression
//! > > MUST be "none".
//!
//! > `random padding`
//! > > Arbitrary-length padding, such that the total length of
//! > > (`packet_length` || `padding_length` || `payload` || `random padding`)
//! > > is a multiple of the cipher block size or 8, whichever is
//! > > larger.  There MUST be at least four bytes of padding.  The
//! > > padding SHOULD consist of random bytes.  The maximum amount of
//! > > padding is 255 bytes.
//!
//! > `mac`
//! > > Message Authentication Code.  If message authentication has
//! > > been negotiated, this field contains the MAC bytes.  Initially,
//! > > the MAC algorithm MUST be "none".
//!
//! Note that the length of the concatenation of `packet_length`,
//! `padding_length`, `payload`, and `random padding` MUST be a multiple
//! of the cipher block size or 8, whichever is larger.  This constraint
//! MUST be enforced, even when using stream ciphers.  Note that the
//! `packet_length` field is also encrypted, and processing it requires
//! special care when sending or receiving packets.  Also note that the
//! insertion of variable amounts of `random padding` may help thwart
//! traffic analysis.
//!
//! The minimum size of a packet is 16 (or the cipher block size,
//! whichever is larger) bytes (plus `mac`).  Implementations SHOULD
//! decrypt the length after receiving the first 8 (or cipher block size,
//! whichever is larger) bytes of a packet.
//!
//! [rfc4253]: https://tools.ietf.org/html/rfc4253#section-6

use GoopyResult;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use error::GoopyErr;
use message::{self, Payload, SshPayload};
use rand::{self, Rng};
use rand::distributions::{IndependentSample, Range};
use std::io::Cursor;
use types::ConnProps;

#[cfg_attr(feature="clippy", allow(cast_possible_truncation))]
fn padding_length(payload_len: usize, cbs: u8) -> u8 {
    let block_size = cbs as usize;
    // Padding min is a number between 0 and (block_size - 1).  The 5 below represents the size in
    // bytes of the packet length value (u32 or 4 bytes) and the padding length value (1 byte).
    let padding_min = (block_size - 1) - (5 + payload_len) % block_size;
    // The minimum padding is 4, so set the min_factor to 1 in this case of padding_min < 4.
    let min_factor = if padding_min < 4 { 1 } else { 0 };
    // Setup some rng to pick a factor between (0 or 1) and (256 / block_size).
    let between = Range::new(min_factor, 256 / block_size);
    let mut rng = rand::thread_rng();
    // This calculates a padding length between 4 and 255
    (padding_min + (block_size * between.ind_sample(&mut rng))) as u8
}

#[cfg_attr(feature="clippy", allow(cast_possible_truncation))]
/// Convert a payload into a vector of bytes as defined in [RFC4253 Section 6][rfc4253-6].
///
/// The values in `NewKeys` are determined after a successful key exchange and determine the values
/// for cipher block size and mac, used when creating an ssh packet.  If the key exchange hasn't
/// happened yet, the minimum 8 is used for cipher block size, and mac is assumed to be "none".
/// [rfc4253-6]: https://tools.ietf.org/html/rfc4253#section-6
pub fn as_bytes(payload: Vec<u8>, conn_props: Option<ConnProps>) -> GoopyResult<Vec<u8>> {
    let payload_len = payload.len();
    let cbs = match conn_props {
        Some(_) | None => 8,
    };
    let padding_len = padding_length(payload_len, cbs);
    let packet_len = (payload_len + padding_len as usize + 1) as u32;
    let mut padding = Vec::with_capacity(padding_len as usize);

    let mut rng = rand::thread_rng();
    for _ in 0..padding_len {
        padding.push(rng.gen::<u8>());
    }

    // Calculate the MAC here when implemented.
    let mac = match conn_props {
        Some(_) => vec![],
        None => vec![],
    };

    let mut packet = Vec::new();
    try!(packet.write_u32::<BigEndian>(packet_len));
    packet.push(padding_len);
    packet.extend_from_slice(&payload);
    packet.extend_from_slice(&padding);
    packet.extend_from_slice(&mac);
    Ok(packet)
}

/// Convert a vector of bytes into an `SshPayload`.
pub fn into_payload(bytes: Vec<u8>, conn_props: Option<ConnProps>) -> GoopyResult<SshPayload> {
    use message::SshPayload::*;
    use message::{debug, disconnect, ignored, kexinit, newkeys, reserved, svcacc, svcreq};

    let mut bytes_iter = if let Some(_nk) = conn_props {
        // TODO: Decrypt the bytes here with the encryption algo.
        vec![].into_iter()
    } else {
        bytes.into_iter()
    };

    // Get the Packet Length as u32
    let plv = bytes_iter.by_ref().take(4).collect::<Vec<u8>>();
    let mut rdr = Cursor::new(&plv);
    let packet_length = try!(rdr.read_u32::<BigEndian>());

    // Get the packet and mac.
    let packet = bytes_iter.by_ref().take(packet_length as usize).collect::<Vec<u8>>();
    let mut mac = Vec::new();
    mac.extend(bytes_iter);

    // TODO: Verify the MAC on packet and fail if it doesn't match.  Otherwise, generate
    // the SshPayload.

    // If the MAC is valid for the packet generate an SshPayload.
    let mut pkt_iter = packet.into_iter();
    // Get the Padding Length
    if let Some(pl) = pkt_iter.next() {
        // Calculate the payload length
        let payload_length = packet_length - (pl as u32) - 1;

        // Grab the payload
        let payload = pkt_iter.by_ref().take(payload_length as usize).collect::<Vec<u8>>();
        match payload.get(0) {
            Some(&message::SSH_MSG_DISCONNECT) => {
                Ok(Disconnect(try!(<disconnect::Message as Payload>::from_bytes(payload))))
            }
            Some(&message::SSH_MSG_IGNORE) => {
                Ok(IgnoredData(try!(<ignored::Message as Payload>::from_bytes(payload))))
            }
            Some(&message::SSH_MSG_UNIMPLEMENTED) => {
                Ok(Unimplemented(try!(<reserved::Message as Payload>::from_bytes(payload))))
            }
            Some(&message::SSH_MSG_DEBUG) => {
                Ok(Debug(try!(<debug::Message as Payload>::from_bytes(payload))))
            }
            Some(&message::SSH_MSG_SERVICE_REQUEST) => {
                Ok(ServiceRequest(try!(<svcreq::Message as Payload>::from_bytes(payload))))
            }
            Some(&message::SSH_MSG_SERVICE_ACCEPT) => {
                Ok(ServiceAccept(try!(<svcacc::Message as Payload>::from_bytes(payload))))
            }
            Some(&message::SSH_MSG_KEXINIT) => {
                Ok(KeyExchangeInit(try!(<kexinit::Message as Payload>::from_bytes(payload))))
            }
            Some(&message::SSH_MSG_NEWKEYS) => {
                Ok(NewKeys(try!(<newkeys::Message as Payload>::from_bytes(payload))))
            }
            Some(&m) => Err(GoopyErr::UnknownMessage(m)),
            None => Err(GoopyErr::UnknownMessage(0)),
        }
    } else {
        Err(GoopyErr::PaddingLength)
    }
}
