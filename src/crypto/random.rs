//! The library provides a set of functions to generate unpredictable data, suitable for creating
//! secret keys.
//!
//! * On Windows systems, the `RtlGenRandom` function is used
//! * On `OpenBSD` and Bitrig, the `arc4random` function is used
//! * On recent Linux kernels, the `getrandom` system call is used (since Sodium 1.0.3)
//! * On other Unices, the `/dev/urandom` device is used
//!
//! If none of these options can safely be used, custom implementations can easily be hooked.

use libc::{c_void, size_t, uint32_t};

extern "C" {
    fn randombytes_buf(buf: *mut c_void, size: size_t) -> ();
    fn randombytes_close() -> ();
    fn randombytes_random() -> uint32_t;
    fn randombytes_stir() -> ();
    fn randombytes_uniform(upper_bound: uint32_t) -> uint32_t;
}

/// The `buf` function fills the given mutable byte array with an unpredictable sequence of bytes.
///
/// The `buf` function safely wrap the `randombytes_buf` function.
///
/// # Examples
///
/// ```
/// use goopy::crypto::random;
///
/// let mut ra0 = [0; 16];
/// random::buf(&mut ra0);
/// assert!(ra0 != [0; 16]);
/// ```
pub fn buf(buf: &mut [u8]) {
    unsafe {
        randombytes_buf(buf.as_mut_ptr() as *mut c_void, buf.len() as size_t);
    }
}

/// This deallocates the global resources used by the pseudo-random number generator. More
/// specifically, when the /dev/urandom device is used, it closes the descriptor. Explicitly
/// calling this function is almost never required.
///
/// # Examples
///
/// ```
/// use goopy::crypto::random;
///
/// let mut ra0 = [0; 16];
/// random::buf(&mut ra0);
/// assert!(ra0 != [0; 16]);
/// random::close();
/// ```
pub fn close() {
    unsafe {
        randombytes_close();
    }
}

/// The `rand` function returns an unpredictable value between 0 and 0xffffffff (included).
///
/// The `rand` function safely wraps the `randombytes_random()` function.
// .
/// # Examples
///
/// ```
/// use goopy::crypto::random;
///
/// let r0 = random::rand();
/// let r1 = random::rand();
/// assert!(r0 != r1);
/// ```
pub fn rand() -> u32 {
    unsafe { randombytes_random() }
}

/// The `stir` function reseeds the pseudo-random number generator, if it supports this operation.
/// Calling this function is not required with the default generator, even after a `fork` call,
/// unless the descriptor for /dev/urandom was closed using `close`.
///
/// If a non-default implementation is being used (see `randombytes_set_implementation`), `stir`
/// must be called by the child after a `fork` call.
// .
/// # Examples
///
/// ```
/// use goopy::crypto::random;
///
/// let r0 = random::rand();
/// let r1 = random::rand();
/// assert!(r0 != r1);
/// random::stir();
/// ```
pub fn stir() {
    unsafe { randombytes_stir() }
}

/// The `uniform` function returns an unpredictable value between 0 and `upper_bound` (excluded).
/// Unlike `random` % `upper_bound`, it does its best to guarantee a uniform distribution of the
/// possible output values.
///
/// The `uniform` function safely wraps the `randombytes_uniform()` function.
///
/// # Examples
///
/// ```
/// use goopy::crypto::random;
///
/// let r0 = random::uniform(10);
/// assert!(r0 < 10);
/// ```
pub fn uniform(upper_bound: u32) -> u32 {
    unsafe { randombytes_uniform(upper_bound) }
}
