//! Bindings to libsodium
//!
//! `Sodium` is a modern, easy-to-use software library for encryption, decryption, signatures,
//! password hashing and more.
//!
//! It is a portable, cross-compilable, installable, packageable fork of `NaCl`, with a compatible
//! API, and an extended API to improve usability even further.
//!
//! Its goal is to provide all of the core operations needed to build higher-level cryptographic
//! tools.
//!
//! Sodium supports a variety of compilers and operating systems, including Windows (with `MinGW`
//! or `Visual Studio`, `x86` and `x86_64`), `iOS` and `Android`.
//!
//! The design choices emphasize security, and "magic constants" have clear rationales.  And
//! despite the emphasis on high security, primitives are faster across-the-board than most
//! implementations of the NIST standards.

use libc::c_int;

pub mod mem;
pub mod random;
pub mod scalar;
pub mod utils;

extern "C" {
    fn sodium_init() -> c_int;
}

/// `init()` safely wraps `sodium_init()` described below.
///
/// `sodium_init()` initializes the library and should be called before any other
/// function provided by libsodium. The function can be called more than once,
/// but it should not be executed by multiple threads simultaneously. Add
/// appropriate locks around the function call if this scenario can happen in
/// your application.
///
/// After this function returns, all of the other functions provided by
/// libsodium will be thread-safe.
///
/// `sodium_init()` doesn't perform any memory allocations. However, on Unix
/// systems, it opens */dev/urandom* and keeps the descriptor open so that the
/// device remains accessible after a *chroot()* call. Multiple calls to
/// `sodium_init()` do not cause additional descriptors to be opened.
///
/// # Examples
///
/// ```
/// use goopy::crypto;
///
/// assert!(crypto::init() == 0);
/// ```
///
pub fn init() -> i32 {
    unsafe { sodium_init() }
}
