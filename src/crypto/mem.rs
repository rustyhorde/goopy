//! goopy crypto memory

use error::CryptoErr::{self, LengthNotEqual, MemLock, MemProtect, NullPtr};
use libc::{c_int, c_void, size_t};
use std::slice;

extern "C" {
    fn sodium_allocarray(count: size_t, size: size_t) -> *mut c_void;
    fn sodium_free(ptr: *mut c_void) -> ();
    fn sodium_malloc(size: size_t) -> *mut c_void;
    fn sodium_memcmp(ptr1: *const c_void, ptr2: *const c_void, size: size_t) -> c_int;
    fn sodium_memzero(ptr: *const c_void, size: size_t) -> ();
    fn sodium_mlock(ptr: *const c_void, size: size_t) -> c_int;
    fn sodium_mprotect_noaccess(ptr: *mut c_void) -> c_int;
    fn sodium_mprotect_readonly(ptr: *mut c_void) -> c_int;
    fn sodium_mprotect_readwrite(ptr: *mut c_void) -> c_int;
    fn sodium_munlock(ptr: *const c_void, size: size_t) -> c_int;
}

/// The `alloc_array` function returns a pointer from which count objects that are size bytes of
/// memory each can be accessed.
///
/// It provides the same guarantees as `malloc` but also protects against arithmetic overflows when
/// `count` * `size` exceeds `SIZE_MAX`.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem};
///
/// let _ = crypto::init();
/// let mut v = mem::alloc_array(5, 64).unwrap();
/// assert!(v.len() == 5 * 64);
/// mem::free(v);
/// ```
pub fn alloc_array<'a>(count: usize, size: usize) -> Result<&'a mut [u8], CryptoErr> {
    let ptr = unsafe { sodium_allocarray(count, size) as *mut u8 };

    if ptr.is_null() {
        Err(NullPtr)
    } else {
        Ok(unsafe { slice::from_raw_parts_mut(ptr, size * count) })
    }
}

/// When a comparison involves secret data (e.g. key, authentication tag), is it critical to use a
/// constant-time comparison function in order to mitigate side-channel attacks.
///
/// The `cmp` function can be used for this purpose.
///
/// The function returns true if the bytes pointed to by b1 match the bytes pointed to by b2.
/// Otherwise, it returns false.
///
/// **Note**: `cmp` is not a lexicographic comparator and is not a generic replacement for
/// `memcmp`.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem};
///
/// let _ = crypto::init();
/// let mut v1 = mem::malloc(16).unwrap();
/// v1[0] = 1;
/// v1[15] = 255;
/// let mut v2 = mem::malloc(16).unwrap();
/// v2[0] = 1;
/// v2[15] = 255;
/// let v3 = mem::malloc(15).unwrap();
/// assert!(v1.len() == 16);
/// assert!(v2.len() == 16);
/// assert!(mem::cmp(v1, v2).unwrap());
/// assert!(mem::cmp(v1, v3).is_err());
/// mem::free(v1);
/// mem::free(v2);
/// mem::free(v3);
/// ```
pub fn cmp(b1: &[u8], b2: &[u8]) -> Result<bool, CryptoErr> {
    if b1.len() == b2.len() {
        let res;
        unsafe {
            res = sodium_memcmp(b1.as_ptr() as *const c_void,
                                b2.as_ptr() as *const c_void,
                                b1.len());
        }

        if res == 0 { Ok(true) } else { Ok(false) }
    } else {
        Err(LengthNotEqual)
    }
}

/// The `free` function unlocks and deallocates memory allocated using `malloc` or `allocarray`.
///
/// Prior to this, the canary is checked in order to detect possible buffer underflows and
/// terminate the process if required.
///
/// `free` also fills the memory region with zeros before the deallocation.
///
/// This function can be called even if the region was previously protected using
/// `mprotect_readonly` the protection will automatically be changed as needed.
///
/// The `free` function wraps the unsafe `sodium_free` function.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem};
///
/// let _ = crypto::init();
/// let mut v = mem::malloc(128).unwrap();
/// v[0] = 1;
/// v[127] = 255;
/// assert!(v.len() == 128);
/// assert!(v[0] == 1);
/// assert!(v[127] == 255);
/// mem::free(v);
/// ```
pub fn free(mem: &[u8]) {
    unsafe {
        sodium_free(mem.as_ptr() as *mut c_void);
    }
}

/// The `lock` function locks the bytes of memory. This can help avoid swapping sensitive data to
/// disk.
///
/// In addition, it is recommended to totally disable swap partitions on machines processing
/// senstive data, or, as a second choice, use encrypted swap partitions.
///
/// For similar reasons, on Unix systems, one should also disable core dumps when running crypto
/// code outside a development environment. This can be achieved using a shell built-in such as
/// `ulimit` or programatically using `setrlimit(RLIMIT_CORE, &(struct rlimit) {0, 0})`. On
/// operating systems where this feature is implemented, kernel crash dumps should also be
/// disabled.
///
/// `lock` wraps the unsafe `sodium_mlock` which itself wraps `mlock` and `VirtualLock`. Note: Many
/// systems place limits on the amount of memory that may be locked by a process. Care should be
/// taken to raise those limits (e.g. Unix ulimits) where neccessary. `lock` will return -1 when
/// any limit is reached.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem};
///
/// let _ = crypto::init();
/// let mut v = mem::malloc(128).unwrap();
/// assert!(mem::lock(v).is_ok());
/// v[0] = 1;
/// v[127] = 255;
/// assert!(v.len() == 128);
/// assert!(v[0] == 1);
/// assert!(v[127] == 255);
/// mem::free(v);
/// ```
#[cfg_attr(feature="clippy", allow(cast_sign_loss))]
pub fn lock(mem: &[u8]) -> Result<u32, CryptoErr> {
    let res;
    unsafe {
        res = sodium_mlock(mem.as_ptr() as *const c_void, mem.len() as size_t);
    }

    if res == 0 {
        Ok(res as u32)
    } else {
        Err(MemLock(mem.len()))
    }
}

/// The `malloc` function returns a mutable array of bytes.
///
/// The allocated region is placed at the end of a page boundary, immediately followed by a guard
/// page. As a result, accessing memory past the end of the region will immediately terminate the
/// application.
///
/// A canary is also placed right before the returned pointer. Modification of this canary are
/// detected when trying to free the allocated region with `free`, and also cause the application
/// to immediately terminate.
///
/// An additional guard page is placed before this canary to make it less likely for sensitive data
/// to be accessible when reading past the end of an unrelated region.
///
/// The allocated region is filled with 0xd0 bytes in order to help catch bugs due to initialized
/// data.
///
/// In addition, `sodium_mlock` is called on the region to help avoid it being swapped to disk. On
/// operating systems supporting `MAP_NOCORE` or `MADV_DONTDUMP`, memory allocated this way will
/// also not be part of core dumps.
///
/// The returned address will not be aligned if the allocation size is not a multiple of the
/// required alignment.
///
/// For this reason, `malloc` should not be used with packed or variable-length structures, unless
/// the size given to `malloc` is rounded up in order to ensure proper alignment.
///
/// All the structures used by libsodium can safely be allocated using `malloc`, the only one
/// requiring extra care being `crypto_generichash_state`, whose size needs to be rounded up to a
/// multiple of 64 bytes.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem};
///
/// let _ = crypto::init();
/// let mut v = mem::malloc(64).unwrap();
/// v[0] = 1;
/// assert!(v.len() == 64);
/// assert!(v[0] == 1);
/// mem::free(v);
/// ```
pub fn malloc<'a>(size: usize) -> Result<&'a mut [u8], CryptoErr> {
    let ptr = unsafe { sodium_malloc(size as size_t) as *mut u8 };

    if ptr.is_null() {
        Err(NullPtr)
    } else {
        Ok(unsafe { slice::from_raw_parts_mut(ptr, size) })
    }
}

/// The `no_access` function makes a region allocated using `malloc` or `allocarray` inaccessible.
/// It cannot be read or written, but the data are preserved.
///
/// This function can be used to make confidential data inacessible except when actually needed for
/// a specific operation.
///
/// `no_access` wraps the unsafe `sodium_mprotect_noaccess()` function.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem};
///
/// let _ = crypto::init();
/// let mut v = mem::malloc(64).unwrap();
/// v[0] = 1;
/// assert!(v.len() == 64);
/// assert!(v[0] == 1);
/// assert!(mem::no_access(&v).is_ok());
/// // If you uncomment the following line the program will fail (no read).
/// // assert!(v[0] == 1);
/// // If you uncomment the following line the program will fail (no write).
/// // v[1] = 1;
/// mem::free(&v);
/// ```
#[cfg_attr(feature="clippy", allow(cast_sign_loss))]
pub fn no_access(mem: &[u8]) -> Result<u32, CryptoErr> {
    let res;
    unsafe {
        res = sodium_mprotect_noaccess(mem.as_ptr() as *mut c_void);
    }

    if res == 0 {
        Ok(res as u32)
    } else {
        Err(MemLock(mem.len()))
    }
}

/// The `readonly` function marks a region allocated using `malloc` or `allocarray` as read-only.
///
/// Attempting to modify the data will cause the process to terminate.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem};
///
/// let _ = crypto::init();
/// let mut v = mem::malloc(64).unwrap();
/// v[0] = 1;
/// assert!(v.len() == 64);
/// assert!(v[0] == 1);
/// assert!(mem::no_access(&mut v).is_ok());
/// assert!(mem::readwrite(&mut v).is_ok());
/// assert!(v[0] == 1);
/// v[0] = 2;
/// assert!(v[0] == 2);
/// mem::free(&mut v);
/// ```
#[cfg_attr(feature="clippy", allow(cast_sign_loss))]
pub fn readonly(mem: &mut [u8]) -> Result<u32, CryptoErr> {
    let res;
    unsafe {
        res = sodium_mprotect_readonly(mem.as_ptr() as *mut c_void);
    }

    if res == 0 {
        Ok(res as u32)
    } else {
        Err(MemLock(mem.len()))
    }
}

/// The `readwrite` function marks a region allocated using `malloc` or `alloc_array` as readable
/// and writable, after having been protected using `readonly` or `no_access`.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem};
///
/// let _ = crypto::init();
/// let mut v = mem::malloc(64).unwrap();
/// v[0] = 1;
/// assert!(v.len() == 64);
/// assert!(v[0] == 1);
/// assert!(mem::readonly(&mut v).is_ok());
/// assert!(v[0] == 1);
/// // If you uncomment the following line the program will fail (no write).
/// // v[1] = 1;
/// mem::free(&mut v);
/// ```
#[cfg_attr(feature="clippy", allow(cast_sign_loss))]
pub fn readwrite(mem: &mut [u8]) -> Result<u32, CryptoErr> {
    let res;
    unsafe {
        res = sodium_mprotect_readwrite(mem.as_ptr() as *mut c_void);
    }

    if res == 0 {
        Ok(res as u32)
    } else {
        Err(MemProtect)
    }
}

/// The 'unlock' function should be called after locked memory is not being used any more. It will
/// zero the bytes before actually flagging the pages as swappable again. Calling `zero` prior to
/// `unlock` is thus not required.+
///
/// On systems where it is supported, `sodium_mlock` also wraps `madvise` and advises the kernel
/// not to include the locked memory in core dumps. `sodium_unlock` also undoes this additional
/// protection.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem};
///
/// let _ = crypto::init();
/// let mut v = mem::malloc(128).unwrap();
/// assert!(mem::lock(v).is_ok());
/// v[0] = 1;
/// v[127] = 255;
/// assert!(v.len() == 128);
/// assert!(v[0] == 1);
/// assert!(v[127] == 255);
/// assert!(mem::unlock(v).is_ok());
/// mem::free(v);
/// ```
#[cfg_attr(feature="clippy", allow(cast_sign_loss))]
pub fn unlock(mem: &[u8]) -> Result<u32, CryptoErr> {
    let res;
    unsafe {
        res = sodium_munlock(mem.as_ptr() as *const c_void, mem.len() as size_t);
    }

    if res == 0 {
        Ok(res as u32)
    } else {
        Err(MemLock(mem.len()))
    }
}

/// After use, sensitive data should be overwritten, but `memset` and hand-written code can be
/// silently stripped out by an optimizing compiler or by the linker.
///
/// The `zero` function tries to effectively zero the bytes, even if optimizations are being
/// applied to the code.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem, random};
///
/// let _ = crypto::init();
/// let mut v = mem::malloc(16).unwrap();
/// random::buf(&mut v);
/// assert!(v.len() == 16);
/// assert!(v != [0; 16]);
/// mem::zero(v);
/// assert!(v == [0; 16]);
/// mem::free(v);
/// ```
pub fn zero(mem: &[u8]) {
    unsafe {
        sodium_memzero(mem.as_ptr() as *const c_void, mem.len());
    }
}
