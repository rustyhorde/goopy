//! libsodium scalarmult bindings

use error::CryptoErr::{self, ByteArrayLength};
use libc::{c_int, c_uchar};

/// Secret Key Length in bytes
pub const BYTES: usize = 32;
/// Public Key Length in bytes
pub const SCALARBYTES: usize = 32;

extern "C" {
    fn crypto_scalarmult(q: *mut c_uchar, n: *const c_uchar, p: *const c_uchar) -> c_int;
    fn crypto_scalarmult_base(q: *mut c_uchar, n: *const c_uchar) -> c_int;
}

/// Given a user's secret key n (`SCALARBYTES` bytes) and a group element p (`BYTES` bytes), the
/// `mult` function computes the user's public key and puts it into q (`BYTES` bytes). `BYTES` and
/// `SCALARBYTES` are provided for consistency, but it is safe to assume that `BYTES` ==
/// `SCALARBYTES`.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem, random, scalar};
///
/// let _ = crypto::init();
/// let mut n = mem::malloc(scalar::SCALARBYTES).unwrap();
/// let mut q = mem::malloc(scalar::BYTES).unwrap();
/// let mut p = mem::malloc(scalar::BYTES).unwrap();
/// random::buf(&mut n);
/// random::buf(&mut p);
/// assert!(n != [0; scalar::SCALARBYTES]);
/// assert!(p != [0; scalar::BYTES]);
/// scalar::mult(&mut q, &n, &p);
/// assert!(q != [0; scalar::BYTES]);
/// mem::free(&n);
/// mem::free(&p);
/// mem::free(&q);
/// ```
///
pub fn mult(q: &mut [u8], n: &[u8], p: &[u8]) -> Result<i32, CryptoErr> {
    if q.len() != BYTES {
        return Err(ByteArrayLength(format!("q is not {} bytes", BYTES)));
    }
    if n.len() != SCALARBYTES {
        return Err(ByteArrayLength(format!("n is not {} bytes", SCALARBYTES)));
    }
    if p.len() != BYTES {
        return Err(ByteArrayLength(format!("p is not {} bytes", BYTES)));
    }

    unsafe { Ok(crypto_scalarmult(q.as_mut_ptr(), n.as_ptr(), p.as_ptr())) }
}

/// Given a user's secret key n (`SCALARBYTES` bytes), the `base` function computes the user's
/// public key and puts it into q (`BYTES` bytes). `BYTES` and `SCALARBYTES` are provided for
/// consistency, but it is safe to assume that `BYTES` == `SCALARBYTES`.
///
/// # Examples
///
/// ```
/// use goopy::crypto::{self, mem, random, scalar};
///
/// let _ = crypto::init();
/// let mut n = mem::malloc(scalar::SCALARBYTES).unwrap();
/// let mut q = mem::malloc(scalar::BYTES).unwrap();
/// random::buf(&mut n);
/// assert!(n != [0; scalar::SCALARBYTES]);
/// scalar::mult_base(&mut q, &n);
/// assert!(q != [0; scalar::BYTES]);
/// mem::free(&n);
/// mem::free(&q);
/// ```
///
pub fn mult_base(q: &mut [u8], n: &[u8]) -> Result<i32, CryptoErr> {
    if n.len() != SCALARBYTES {
        return Err(ByteArrayLength(format!("n is not {} bytes", SCALARBYTES)));
    }
    if q.len() != BYTES {
        return Err(ByteArrayLength(format!("q is not {} bytes", BYTES)));
    }

    unsafe { Ok(crypto_scalarmult_base(q.as_mut_ptr(), n.as_ptr())) }
}
