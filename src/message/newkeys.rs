//! New Keys `Message` Packet - [RFC4253 Section 7.3][rfc4253-7.3]
//!
//! ```ignored
//!   byte      SSH_MSG_NEWKEYS
//! ```
//!
//! Key exchange ends by each side sending an `SSH_MSG_NEWKEYS` message.
//! This message is sent with the old keys and algorithms.  All messages
//! sent after this message MUST use the new keys and algorithms.
//!
//! When this message is received, the new keys and algorithms MUST be
//! used for receiving.
//!
//! The purpose of this message is to ensure that a party is able to
//! respond with an `SSH_MSG_DISCONNECT` message that the other party can
//! understand if something goes wrong with the key exchange.
//!
//! [rfc4253-7.3]: https://tools.ietf.org/html/rfc4253#section-7.3

use GoopyResult;
use error::GoopyErr;
use message::{Payload, SSH_MSG_NEWKEYS};

/// New Keys `Message` Packet
pub struct Message {
    id: u8,
}

impl Default for Message {
    fn default() -> Message {
        Message { id: SSH_MSG_NEWKEYS }
    }
}

impl Payload for Message {
    /// Convert the given `Message` struct into a vector of bytes.
    fn into_bytes(self) -> GoopyResult<Vec<u8>> {
        let mut db_bytes = Vec::new();
        db_bytes.push(self.id);
        Ok(db_bytes)
    }

    /// Convert a vector of bytes into a `Message` struct.
    fn from_bytes(bytes: Vec<u8>) -> GoopyResult<Message> {
        let mut iter = bytes.into_iter();

        // Get the Message Type Byte
        let id = if let Some(mb) = iter.by_ref().next() {
            mb
        } else {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        };

        if id != SSH_MSG_NEWKEYS {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        }

        Ok(Message { id: id })
    }
}

#[cfg(test)]
mod nkm_test {
    use libc::{c_void, memcmp};
    use message::{Payload, SSH_MSG_NEWKEYS};
    use std::default::Default;
    use super::Message;

    const TEST_NKM_BYTES: [u8; 1] = [21];

    #[test]
    fn default_nkm() {
        let nkm: Message = Default::default();
        assert!(SSH_MSG_NEWKEYS == nkm.id);
    }

    #[test]
    fn nkm_into_bytes() {
        let nkm: Message = Default::default();

        if let Ok(bytes) = nkm.into_bytes() {
            let len = TEST_NKM_BYTES.len();
            let t_ptr = TEST_NKM_BYTES.as_ptr() as *const c_void;
            let b_ptr = bytes.as_ptr() as *const c_void;
            unsafe {
                assert!(0 == memcmp(t_ptr, b_ptr, len));
            }
        } else {
            assert!(false);
        }
    }

    #[test]
    fn nkm_from_bytes() {
        let mut test_vec = Vec::new();
        test_vec.extend_from_slice(&TEST_NKM_BYTES);
        if let Ok(nkm) = Message::from_bytes(test_vec) {
            assert!(nkm.id == SSH_MSG_NEWKEYS);
        } else {
            assert!(false);
        }
    }
}
