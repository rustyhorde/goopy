//! Service Accept `Message` Packet - [RFC4253 Section 10][rfc4253-10]
//!
//! ```ignored
//!   byte      SSH_MSG_SERVICE_ACCEPT
//!   string    service name
//! ```
//!
//! If the server supports the service (and permits the client to use
//! it), it MUST respond with this message.
//!
//! Message numbers used by services should be in the area reserved for
//! them (see [SSH-ARCH][] and [SSH-NUMBERS][]).  The transport level will
//! continue to process its own messages.
//!
//! [rfc4253-10]: https://tools.ietf.org/html/rfc4253#section-10
//! [ssh-arch]: https://tools.ietf.org/html/rfc4251
//! [ssh-numbers]: https://tools.ietf.org/html/rfc4250

use GoopyResult;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use error::GoopyErr;
use message::{Payload, SSH_MSG_SERVICE_ACCEPT};
use std::io::Cursor;

/// Service Accept `Message` Packet
pub struct Message {
    id: u8,
    service_name: String,
}

impl Message {
    /// Create a new `Message` with the given sequence number
    pub fn new(service_name: String) -> Message {
        Message {
            id: SSH_MSG_SERVICE_ACCEPT,
            service_name: service_name,
        }
    }
}

impl Payload for Message {
    /// Convert the given `Message` struct into a vector of bytes.
    fn into_bytes(self) -> GoopyResult<Vec<u8>> {
        let mut db_bytes = Vec::new();
        db_bytes.push(self.id);
        try!(db_bytes.write_u32::<BigEndian>(self.service_name.len() as u32));
        db_bytes.extend_from_slice(&self.service_name.into_bytes());
        Ok(db_bytes)
    }

    /// Convert a vector of bytes into a `Message` struct.
    fn from_bytes(bytes: Vec<u8>) -> GoopyResult<Message> {
        let mut iter = bytes.into_iter();

        // Get the Message Type Byte
        let id = if let Some(mb) = iter.by_ref().next() {
            mb
        } else {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        };

        if id != SSH_MSG_SERVICE_ACCEPT {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        }

        let mut service_name_cursor = Cursor::new(iter.by_ref().take(4).collect::<Vec<u8>>());
        let service_name_len = try!(service_name_cursor.read_u32::<BigEndian>());
        let service_name_bytes: Vec<u8> = iter.by_ref().take(service_name_len as usize).collect();
        let service_name = String::from_utf8_lossy(&service_name_bytes).into_owned();

        Ok(Message {
            id: id,
            service_name: service_name,
        })
    }
}

#[cfg(test)]
mod svcam_test {


    use libc::{c_void, memcmp};
    use message::{Payload, SSH_MSG_SERVICE_ACCEPT};
    use super::Message;

    const TEST_SVCAM_BYTES: [u8; 17] = [6, 0, 0, 0, 12, 115, 115, 104, 45, 117, 115, 101, 114, 97,
                                        117, 116, 104];

    #[test]
    fn new_svcam() {
        let svcam = Message::new("ssh-userauth".to_string());
        assert!(SSH_MSG_SERVICE_ACCEPT == svcam.id);
        assert!("ssh-userauth" == &svcam.service_name);
    }

    #[test]
    fn svcam_into_bytes() {
        let svcam = Message::new("ssh-userauth".to_string());

        if let Ok(bytes) = svcam.into_bytes() {
            let len = TEST_SVCAM_BYTES.len();
            let t_ptr = TEST_SVCAM_BYTES.as_ptr() as *const c_void;
            let b_ptr = bytes.as_ptr() as *const c_void;
            unsafe {
                assert!(0 == memcmp(t_ptr, b_ptr, len));
            }
        } else {
            assert!(false);
        }
    }

    #[test]
    fn svcam_from_bytes() {
        let mut test_vec = Vec::new();
        test_vec.extend_from_slice(&TEST_SVCAM_BYTES);
        if let Ok(svcam) = Message::from_bytes(test_vec) {
            assert!(svcam.id == SSH_MSG_SERVICE_ACCEPT);
            assert!(&svcam.service_name == "ssh-userauth")
        } else {
            assert!(false);
        }
    }
}
