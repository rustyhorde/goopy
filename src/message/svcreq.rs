//! Service Request `Message` Packet - [RFC4253 Section 10][rfc4253-10]
//!
//! ```ignored
//!   byte      SSH_MSG_SERVICE_REQUEST
//!   string    service name
//! ```
//!
//! After the key exchange, the client requests a service.  The service
//! is identified by a name.  The format of names and procedures for
//! defining new names are defined in [SSH-ARCH][] and [SSH-NUMBERS][].
//!
//! If the server rejects the service request, it SHOULD send an
//! appropriate `SSH_MSG_DISCONNECT` message and MUST disconnect.
//!
//! When the service starts, it may have access to the session identifier
//! generated during the key exchange.
//!
//! [rfc4253-10]: https://tools.ietf.org/html/rfc4253#section-10
//! [ssh-arch]: https://tools.ietf.org/html/rfc4251
//! [ssh-numbers]: https://tools.ietf.org/html/rfc4250

use GoopyResult;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use error::GoopyErr;
use message::{Payload, SSH_MSG_SERVICE_REQUEST};
use std::io::Cursor;

/// Service Request `Message` Packet
pub struct Message {
    id: u8,
    service_name: String,
}

impl Message {
    /// Create a new `Message` with the given sequence number
    pub fn new(service_name: String) -> Message {
        Message {
            id: SSH_MSG_SERVICE_REQUEST,
            service_name: service_name,
        }
    }
}

impl Payload for Message {
    /// Convert the given `Message` struct into a vector of bytes.
    fn into_bytes(self) -> GoopyResult<Vec<u8>> {
        let mut db_bytes = Vec::new();
        db_bytes.push(self.id);
        try!(db_bytes.write_u32::<BigEndian>(self.service_name.len() as u32));
        db_bytes.extend_from_slice(&self.service_name.into_bytes());
        Ok(db_bytes)
    }

    /// Convert a vector of bytes into a `Message` struct.
    fn from_bytes(bytes: Vec<u8>) -> GoopyResult<Message> {
        let mut iter = bytes.into_iter();

        // Get the Message Type Byte
        let id = if let Some(mb) = iter.by_ref().next() {
            mb
        } else {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        };

        if id != SSH_MSG_SERVICE_REQUEST {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        }

        let mut service_name_cursor = Cursor::new(iter.by_ref().take(4).collect::<Vec<u8>>());
        let service_name_len = try!(service_name_cursor.read_u32::<BigEndian>());
        let service_name_bytes: Vec<u8> = iter.by_ref().take(service_name_len as usize).collect();
        let service_name = String::from_utf8_lossy(&service_name_bytes).into_owned();

        Ok(Message {
            id: id,
            service_name: service_name,
        })
    }
}

#[cfg(test)]
mod svcrm_test {


    use libc::{c_void, memcmp};
    use message::{Payload, SSH_MSG_SERVICE_REQUEST};
    use super::Message;

    const TEST_SVCRM_BYTES: [u8; 17] = [5, 0, 0, 0, 12, 115, 115, 104, 45, 117, 115, 101, 114, 97,
                                        117, 116, 104];

    #[test]
    fn new_svcrm() {
        let svcrm = Message::new("ssh-userauth".to_string());
        assert!(SSH_MSG_SERVICE_REQUEST == svcrm.id);
        assert!("ssh-userauth" == &svcrm.service_name);
    }

    #[test]
    fn svcrm_into_bytes() {
        let svcrm = Message::new("ssh-userauth".to_string());

        if let Ok(bytes) = svcrm.into_bytes() {
            let len = TEST_SVCRM_BYTES.len();
            let t_ptr = TEST_SVCRM_BYTES.as_ptr() as *const c_void;
            let b_ptr = bytes.as_ptr() as *const c_void;
            unsafe {
                assert!(0 == memcmp(t_ptr, b_ptr, len));
            }
        } else {
            assert!(false);
        }
    }

    #[test]
    fn svcrm_from_bytes() {
        let mut test_vec = Vec::new();
        test_vec.extend_from_slice(&TEST_SVCRM_BYTES);
        if let Ok(svcrm) = Message::from_bytes(test_vec) {
            assert!(svcrm.id == SSH_MSG_SERVICE_REQUEST);
            assert!(&svcrm.service_name == "ssh-userauth")
        } else {
            assert!(false);
        }
    }
}
