//! SSH Message constants and traits

use GoopyResult;

pub mod debug;
pub mod disconnect;
pub mod kexinit;
pub mod ignored;
pub mod newkeys;
pub mod reserved;
pub mod svcacc;
pub mod svcreq;

/// Disconnection Message
pub const SSH_MSG_DISCONNECT: u8 = 1;
/// Ignored Data Message
pub const SSH_MSG_IGNORE: u8 = 2;
/// Unimplemented Message
pub const SSH_MSG_UNIMPLEMENTED: u8 = 3;
/// Debug Message
pub const SSH_MSG_DEBUG: u8 = 4;
/// Service Request Message
pub const SSH_MSG_SERVICE_REQUEST: u8 = 5;
/// Service Accept Message
pub const SSH_MSG_SERVICE_ACCEPT: u8 = 6;
/// Key Exchange Init Message
pub const SSH_MSG_KEXINIT: u8 = 20;
/// New Keys Message
pub const SSH_MSG_NEWKEYS: u8 = 21;
/// Curve25519 Key Exchange Init Message
pub const SSH_MSG_KEX_ECDH_INIT: u8 = 30;
/// Curve25519 Key Exchange Reply Message
pub const SSH_MSG_KEX_ECDH_REPLY: u8 = 31;
/// Diffie-Hellman Group Exchange SHA256 Group Message
pub const SSH_MSG_KEX_DH_GEX_GROUP: u8 = 31;
/// Diffie-Hellman Group Exchange SHA256 Init Message
pub const SSH_MSG_KEX_DH_GEX_INIT: u8 = 32;
/// Diffie-Hellman Group Exchange SHA256 Reply Message
pub const SSH_MSG_KEX_DH_GEX_REPLY: u8 = 33;
/// Diffie-Hellman Group Exchange SHA256 Request Message
pub const SSH_MSG_KEX_DH_GEX_REQUEST: u8 = 34;

/// All ssh messages must implement this trait.
pub trait Payload: Sized {
    /// Convert a packet struct into a vector of bytes.
    fn into_bytes(self) -> GoopyResult<Vec<u8>>;
    /// Convert a vector of bytes into a packet struct.
    fn from_bytes(bytes: Vec<u8>) -> GoopyResult<Self>;
}

/// The enumeration of all supported ssh payloads.
pub enum SshPayload {
    /// A Disconnect `Message` payload
    Disconnect(disconnect::Message),
    /// An Ignored Data `Message` payload
    IgnoredData(ignored::Message),
    /// An Reserved `Message` payload
    Unimplemented(reserved::Message),
    /// A Debug `Message` payload
    Debug(debug::Message),
    /// A Service Request `Message` payload
    ServiceRequest(svcreq::Message),
    /// A Service Accept `Message` payload
    ServiceAccept(svcacc::Message),
    /// A Key Exchange Init `Message` payload
    KeyExchangeInit(kexinit::Message),
    /// A New Keys `Message` payload
    NewKeys(newkeys::Message),
}
