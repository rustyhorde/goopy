//! Disconnection `Message` Packet - [RFC4253 Section 11.1][rfc4253-11.1]
//!
//! ```ignored
//!   byte      SSH_MSG_DISCONNECT
//!   uint32    reason code
//!   string    description in ISO-10646 UTF-8 encoding
//!   string    language tag
//! ```
//!
//! This message causes immediate termination of the connection.  All
//! implementations MUST be able to process this message; they SHOULD be
//! able to send this message.
//!
//! The ISO-10646 UTF-8 encoding format for the `description` field is
//! described in [RFC3629][].
//!
//! The `language tag` is described in [RFC3066][].
//!
//! The sender MUST NOT send or receive any data after this message, and
//! the recipient MUST NOT accept any data after receiving this message.
//! The Disconnection Message `description` string gives a more specific
//! explanation in a human-readable form.  The Disconnection Message
//! `reason code` gives the reason in a more machine-readable format
//! (suitable for localization), and can have the values as displayed in
//! the table below.  Note that the decimal representation is displayed
//! in this table for readability, but the values are actually uint32
//! values.
//!
//! ```ignored
//!      Symbolic name                                reason code
//!      -------------                                -----------
//! SSH_DISCONNECT_HOST_NOT_ALLOWED_TO_CONNECT             1
//! SSH_DISCONNECT_PROTOCOL_ERROR                          2
//! SSH_DISCONNECT_KEY_EXCHANGE_FAILED                     3
//! SSH_DISCONNECT_RESERVED                                4
//! SSH_DISCONNECT_MAC_ERROR                               5
//! SSH_DISCONNECT_COMPRESSION_ERROR                       6
//! SSH_DISCONNECT_SERVICE_NOT_AVAILABLE                   7
//! SSH_DISCONNECT_PROTOCOL_VERSION_NOT_SUPPORTED          8
//! SSH_DISCONNECT_HOST_KEY_NOT_VERIFIABLE                 9
//! SSH_DISCONNECT_CONNECTION_LOST                        10
//! SSH_DISCONNECT_BY_APPLICATION                         11
//! SSH_DISCONNECT_TOO_MANY_CONNECTIONS                   12
//! SSH_DISCONNECT_AUTH_CANCELLED_BY_USER                 13
//! SSH_DISCONNECT_NO_MORE_AUTH_METHODS_AVAILABLE         14
//! SSH_DISCONNECT_ILLEGAL_USER_NAME                      15
//! ```
//!
//! If the `description` string is displayed, the control character
//! filtering discussed in [SSH-ARCH][] should be used to avoid attacks by
//! sending terminal control characters.
//!
//! Requests for assignments of new Disconnection Message 'reason code'
//! values (and associated 'description' text) in the range of 0x00000010
//! to 0xFDFFFFFF MUST be done through the IETF CONSENSUS method, as
//! described in [RFC2434][].  The Disconnection Message 'reason code'
//! values in the range of 0xFE000000 through 0xFFFFFFFF are reserved for
//! PRIVATE USE.  As noted, the actual instructions to the IANA are in
//! [SSH-NUMBERS][].
//!
//! [rfc4253-11.1]: https://tools.ietf.org/html/rfc4253#section-11.1
//! [rfc3629]: https://tools.ietf.org/html/rfc3629
//! [rfc3066]: https://tools.ietf.org/html/rfc3066
//! [rfc2434]: https://tools.ietf.org/html/rfc2434
//! [ssh-arch]: https://tools.ietf.org/html/rfc4251
//! [ssh-numbers]: https://tools.ietf.org/html/rfc4250

use GoopyResult;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use error::GoopyErr;
use message::{Payload, SSH_MSG_DISCONNECT};
use std::io::Cursor;

/// Host not allowd to connect
pub const SSH_DISCONNECT_HOST_NOT_ALLOWED_TO_CONNECT: u32 = 1;
/// Protocol error
pub const SSH_DISCONNECT_PROTOCOL_ERROR: u32 = 2;
/// Key Exchange failed
pub const SSH_DISCONNECT_KEY_EXCHANGE_FAILED: u32 = 3;
/// Reserved
pub const SSH_DISCONNECT_RESERVED: u32 = 4;
/// MAC error.
pub const SSH_DISCONNECT_MAC_ERROR: u32 = 5;
/// Compression error.
pub const SSH_DISCONNECT_COMPRESSION_ERROR: u32 = 6;
/// Service not available.
pub const SSH_DISCONNECT_SERVICE_NOT_AVAILABLE: u32 = 7;
/// Protocol version not supported.
pub const SSH_DISCONNECT_PROTOCOL_VERSION_NOT_SUPPORTED: u32 = 8;
/// Host key not verifiable.
pub const SSH_DISCONNECT_HOST_KEY_NOT_VERIFIABLE: u32 = 9;
/// Connection lost.
pub const SSH_DISCONNECT_CONNECTION_LOST: u32 = 10;
/// By application.
pub const SSH_DISCONNECT_BY_APPLICATION: u32 = 11;
/// Too many connections.
pub const SSH_DISCONNECT_TOO_MANY_CONNECTIONS: u32 = 12;
/// Auth cancelled by user.
pub const SSH_DISCONNECT_AUTH_CANCELLED_BY_USER: u32 = 13;
/// No more auth methods available.
pub const SSH_DISCONNECT_NO_MORE_AUTH_METHODS_AVAILABLE: u32 = 14;
/// Illegal username.
pub const SSH_DISCONNECT_ILLEGAL_USER_NAME: u32 = 15;

/// Disconnection `Message` Packet
pub struct Message {
    id: u8,
    reason: u32,
    description: String,
    language: String,
}

impl Message {
    /// Set the `reason` field
    pub fn set_reason(&mut self, reason: u32) -> &mut Message {
        self.reason = reason;
        self
    }

    /// Set the `description` field
    pub fn set_description(&mut self, description: String) -> &mut Message {
        self.description = description;
        self
    }

    /// Set the `language` field
    pub fn set_language(&mut self, language: String) -> &mut Message {
        self.language = language;
        self
    }
}

impl Default for Message {
    fn default() -> Message {
        Message {
            id: SSH_MSG_DISCONNECT,
            reason: 0,
            description: String::new(),
            language: String::new(),
        }
    }
}

impl Payload for Message {
    /// Convert the given `Message` struct into a vector of bytes.
    fn into_bytes(self) -> GoopyResult<Vec<u8>> {
        let mut dc_bytes = Vec::new();
        dc_bytes.push(self.id);
        try!(dc_bytes.write_u32::<BigEndian>(self.reason));
        try!(dc_bytes.write_u32::<BigEndian>(self.description.len() as u32));
        dc_bytes.extend_from_slice(&self.description.into_bytes());
        try!(dc_bytes.write_u32::<BigEndian>(self.language.len() as u32));
        dc_bytes.extend_from_slice(&self.language.into_bytes());
        Ok(dc_bytes)
    }

    /// Convert a vector of bytes into a `Message` struct.
    fn from_bytes(bytes: Vec<u8>) -> GoopyResult<Message> {
        let mut iter = bytes.into_iter();

        // Get the Message Type Byte
        let id = if let Some(mb) = iter.by_ref().next() {
            mb
        } else {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        };

        if id != SSH_MSG_DISCONNECT {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        }

        let mut reason_cursor = Cursor::new(iter.by_ref().take(4).collect::<Vec<u8>>());
        let reason = try!(reason_cursor.read_u32::<BigEndian>());

        let mut desc_cursor = Cursor::new(iter.by_ref().take(4).collect::<Vec<u8>>());
        let desc_len = try!(desc_cursor.read_u32::<BigEndian>());

        let desc_bytes: Vec<u8> = iter.by_ref().take(desc_len as usize).collect();
        let description = String::from_utf8_lossy(&desc_bytes).into_owned();

        let mut lang_cursor = Cursor::new(iter.by_ref().take(4).collect::<Vec<u8>>());
        let lang_len = try!(lang_cursor.read_u32::<BigEndian>());

        let lang_bytes: Vec<u8> = iter.by_ref().take(lang_len as usize).collect();
        let language = String::from_utf8_lossy(&lang_bytes).into_owned();

        Ok(Message {
            id: id,
            reason: reason,
            description: description,
            language: language,
        })
    }
}

#[cfg(test)]
mod dcm_test {


    use libc::{c_void, memcmp};
    use message::{Payload, SSH_MSG_DISCONNECT};
    use std::default::Default;
    use super::{Message, SSH_DISCONNECT_KEY_EXCHANGE_FAILED};

    const TEST_DCM_BYTES: [u8; 37] = [1, 0, 0, 0, 3, 0, 0, 0, 24, 84, 104, 101, 32, 107, 101, 121,
                                      32, 101, 120, 99, 104, 97, 110, 103, 101, 32, 102, 97, 105,
                                      108, 101, 100, 33, 0, 0, 0, 0];

    #[test]
    fn default_dcm() {
        let dcm: Message = Default::default();
        assert!(SSH_MSG_DISCONNECT == dcm.id);
        assert!(dcm.reason == 0);
        assert!(dcm.description.is_empty());
        assert!(dcm.language.is_empty());
    }

    #[test]
    fn dcm_into_bytes() {
        let mut dcm: Message = Default::default();
        dcm.set_reason(SSH_DISCONNECT_KEY_EXCHANGE_FAILED);
        dcm.set_description("The key exchange failed!".to_string());

        if let Ok(bytes) = dcm.into_bytes() {
            let len = TEST_DCM_BYTES.len();
            let t_ptr = TEST_DCM_BYTES.as_ptr() as *const c_void;
            let b_ptr = bytes.as_ptr() as *const c_void;
            unsafe {
                assert!(0 == memcmp(t_ptr, b_ptr, len));
            }
        } else {
            assert!(false);
        }
    }

    #[test]
    fn dcm_from_bytes() {
        let mut test_vec = Vec::new();
        test_vec.extend_from_slice(&TEST_DCM_BYTES);
        if let Ok(dcm) = Message::from_bytes(test_vec) {
            assert!(dcm.id == SSH_MSG_DISCONNECT);
            assert!(dcm.reason == SSH_DISCONNECT_KEY_EXCHANGE_FAILED);
            assert!(&dcm.description == "The key exchange failed!")
        } else {
            assert!(false);
        }
    }
}
