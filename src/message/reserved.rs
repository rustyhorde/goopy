//! Reserved `Message` Packet - [RFC4253 Section 11.4][rfc4253-11.4]
//!
//! ```ignored
//!   byte      SSH_MSG_UNIMPLEMENTED
//!   uint32    packet sequence number of rejected message
//! ```
//!
//! An implementation MUST respond to all unrecognized messages with an
//! `SSH_MSG_UNIMPLEMENTED` message in the order in which the messages were
//! received.  Such messages MUST be otherwise ignored.  Later protocol
//! versions may define other meanings for these message types.
//!
//! [rfc4253-11.4]: https://tools.ietf.org/html/rfc4253#section-11.4

use GoopyResult;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use error::GoopyErr;
use message::{Payload, SSH_MSG_UNIMPLEMENTED};
use std::io::Cursor;

/// Reserved `Message` Packet
pub struct Message {
    id: u8,
    sequence: u32,
}

impl Message {
    /// Create a new `Message` with the given sequence number
    pub fn new(sequence: u32) -> Message {
        Message {
            id: SSH_MSG_UNIMPLEMENTED,
            sequence: sequence,
        }
    }
}

impl Payload for Message {
    /// Convert the given `Message` struct into a vector of bytes.
    fn into_bytes(self) -> GoopyResult<Vec<u8>> {
        let mut db_bytes = Vec::new();
        db_bytes.push(self.id);
        try!(db_bytes.write_u32::<BigEndian>(self.sequence));
        Ok(db_bytes)
    }

    /// Convert a vector of bytes into a `Message` struct.
    fn from_bytes(bytes: Vec<u8>) -> GoopyResult<Message> {
        let mut iter = bytes.into_iter();

        // Get the Message Type Byte
        let id = if let Some(mb) = iter.by_ref().next() {
            mb
        } else {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        };

        if id != SSH_MSG_UNIMPLEMENTED {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        }

        let mut seq_cursor = Cursor::new(iter.by_ref().take(4).collect::<Vec<u8>>());
        let sequence = try!(seq_cursor.read_u32::<BigEndian>());

        Ok(Message {
            id: id,
            sequence: sequence,
        })
    }
}

#[cfg(test)]
mod resm_test {


    use libc::{c_void, memcmp};
    use message::{Payload, SSH_MSG_UNIMPLEMENTED};
    use super::Message;

    const TEST_RESM_BYTES: [u8; 5] = [3, 0, 0, 0, 0];

    #[test]
    fn new_resm() {
        let resm = Message::new(0);
        assert!(SSH_MSG_UNIMPLEMENTED == resm.id);
        assert!(0 == resm.sequence);
    }

    #[test]
    fn resm_into_bytes() {
        let resm = Message::new(0);

        if let Ok(bytes) = resm.into_bytes() {
            let len = TEST_RESM_BYTES.len();
            let t_ptr = TEST_RESM_BYTES.as_ptr() as *const c_void;
            let b_ptr = bytes.as_ptr() as *const c_void;
            unsafe {
                assert!(0 == memcmp(t_ptr, b_ptr, len));
            }
        } else {
            assert!(false);
        }
    }

    #[test]
    fn resm_from_bytes() {
        let mut test_vec = Vec::new();
        test_vec.extend_from_slice(&TEST_RESM_BYTES);
        if let Ok(resm) = Message::from_bytes(test_vec) {
            assert!(resm.id == SSH_MSG_UNIMPLEMENTED);
            assert!(0 == resm.sequence);
        } else {
            assert!(false);
        }
    }
}
