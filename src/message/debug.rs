//! Debug `Message` Packet - [RFC4253 Section 11.3][rfc4253-11.3]
//!
//! ```ignored
//!   byte      SSH_MSG_DEBUG
//!   boolean   always_display
//!   string    message in ISO-10646 UTF-8 encoding
//!   string    language tag
//! ```
//!
//! The ISO-10646 UTF-8 encoding format for the `message` field is
//! described in [RFC3629][].
//!
//! The `language tag` is described in [RFC3066][].
//!
//! All implementations MUST understand this message, but they are
//! allowed to ignore it.  This message is used to transmit information
//! that may help debugging.  If `always_display` is TRUE, the message
//! SHOULD be displayed.  Otherwise, it SHOULD NOT be displayed unless
//! debugging information has been explicitly requested by the user.
//!
//! The `message` doesn't need to contain a newline.  It is, however,
//! allowed to consist of multiple lines separated by CRLF (Carriage
//! Return - Line Feed) pairs.
//!
//! If the `message` string is displayed, the terminal control character
//! filtering discussed in [SSH-ARCH][] should be used to avoid attacks by
//! sending terminal control characters.
//!
//! [rfc4253-11.3]: https://tools.ietf.org/html/rfc4253#section-11.3
//! [rfc3629]: https://tools.ietf.org/html/rfc3629
//! [rfc3066]: https://tools.ietf.org/html/rfc3066
//! [ssh-arch]: https://tools.ietf.org/html/rfc4251

use GoopyResult;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use error::GoopyErr;
use message::{Payload, SSH_MSG_DEBUG};
use std::io::Cursor;

/// Debug `Message` Packet
pub struct Message {
    id: u8,
    always_display: bool,
    message: String,
    language: String,
}

impl Message {
    /// Set the `always_display` field
    pub fn set_always_display(&mut self, always_display: bool) -> &mut Message {
        self.always_display = always_display;
        self
    }

    /// Set the `message` field
    pub fn set_message(&mut self, message: String) -> &mut Message {
        self.message = message;
        self
    }

    /// Set the `language` field
    pub fn set_language(&mut self, language: String) -> &mut Message {
        self.language = language;
        self
    }
}

impl Default for Message {
    fn default() -> Message {
        Message {
            id: SSH_MSG_DEBUG,
            always_display: false,
            message: String::new(),
            language: String::new(),
        }
    }
}

impl Payload for Message {
    /// Convert the given `Message` struct into a vector of bytes.
    fn into_bytes(self) -> GoopyResult<Vec<u8>> {
        let mut db_bytes = Vec::new();
        db_bytes.push(self.id);
        if self.always_display {
            db_bytes.push(0);
        } else {
            db_bytes.push(1);
        }
        try!(db_bytes.write_u32::<BigEndian>(self.message.len() as u32));
        db_bytes.extend_from_slice(&self.message.into_bytes());
        try!(db_bytes.write_u32::<BigEndian>(self.language.len() as u32));
        db_bytes.extend_from_slice(&self.language.into_bytes());
        Ok(db_bytes)
    }

    /// Convert a vector of bytes into a `Message` struct.
    fn from_bytes(bytes: Vec<u8>) -> GoopyResult<Message> {
        let mut iter = bytes.into_iter();

        // Get the Message Type Byte
        let id = if let Some(mb) = iter.by_ref().next() {
            mb
        } else {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        };

        if id != SSH_MSG_DEBUG {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        }

        let always_display = if let Some(ad) = iter.by_ref().next() {
            ad == 0
        } else {
            return Err(GoopyErr::InvalidPacketByte("always-display-byte"));
        };

        let mut msg_cursor = Cursor::new(iter.by_ref().take(4).collect::<Vec<u8>>());
        let msg_len = try!(msg_cursor.read_u32::<BigEndian>());

        let msg_bytes: Vec<u8> = iter.by_ref().take(msg_len as usize).collect();
        let message = String::from_utf8_lossy(&msg_bytes).into_owned();

        let mut lang_cursor = Cursor::new(iter.by_ref().take(4).collect::<Vec<u8>>());
        let lang_len = try!(lang_cursor.read_u32::<BigEndian>());

        let lang_bytes: Vec<u8> = iter.by_ref().take(lang_len as usize).collect();
        let language = String::from_utf8_lossy(&lang_bytes).into_owned();

        Ok(Message {
            id: id,
            always_display: always_display,
            message: message,
            language: language,
        })
    }
}

#[cfg(test)]
mod dbm_test {


    use libc::{c_void, memcmp};
    use message::{Payload, SSH_MSG_DEBUG};
    use std::default::Default;
    use super::Message;

    const TEST_DBM_BYTES: [u8; 33] = [4, 1, 0, 0, 0, 23, 101, 120, 99, 101, 108, 108, 101, 110,
                                      116, 32, 100, 101, 98, 117, 103, 32, 109, 101, 115, 115, 97,
                                      103, 101, 0, 0, 0, 0];

    #[test]
    fn default_dbm() {
        let dbm: Message = Default::default();
        assert!(SSH_MSG_DEBUG == dbm.id);
        assert!(!dbm.always_display);
        assert!(dbm.message.is_empty());
        assert!(dbm.language.is_empty());
    }

    #[test]
    fn dbm_into_bytes() {
        let mut dbm: Message = Default::default();
        dbm.set_message("excellent debug message".to_string());

        if let Ok(bytes) = dbm.into_bytes() {
            let len = TEST_DBM_BYTES.len();
            let t_ptr = TEST_DBM_BYTES.as_ptr() as *const c_void;
            let b_ptr = bytes.as_ptr() as *const c_void;
            unsafe {
                assert!(0 == memcmp(t_ptr, b_ptr, len));
            }
        } else {
            assert!(false);
        }
    }

    #[test]
    fn dbm_from_bytes() {
        let mut test_vec = Vec::new();
        test_vec.extend_from_slice(&TEST_DBM_BYTES);
        if let Ok(dbm) = Message::from_bytes(test_vec) {
            assert!(dbm.id == SSH_MSG_DEBUG);
            assert!(!dbm.always_display);
            assert!(&dbm.message == "excellent debug message")
        } else {
            assert!(false);
        }
    }
}
