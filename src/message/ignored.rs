//! Ignored Data `Message` Packet - [RFC4253 Section 11.2][rfc4253-11.2]
//!
//! ```ignored
//!   byte      SSH_MSG_IGNORE
//!   string    data
//! ```
//!
//! All implementations MUST understand (and ignore) this message at any
//! time (after receiving the identification string).  No implementation
//! is required to send them.  This message can be used as an additional
//! protection measure against advanced traffic analysis techniques.
//!
//! [rfc4253-11.2]: https://tools.ietf.org/html/rfc4253#section-11.2

use GoopyResult;
use error::GoopyErr;
use message::{Payload, SSH_MSG_IGNORE};
use std::default::Default;

/// Ignored Data `Message` Packet
pub struct Message {
    id: u8,
    data: String,
}

impl Message {
    /// Set the `data` field
    pub fn set_data(&mut self, data: String) -> &mut Message {
        self.data = data;
        self
    }
}

impl Default for Message {
    fn default() -> Message {
        Message {
            id: SSH_MSG_IGNORE,
            data: String::new(),
        }
    }
}

impl Payload for Message {
    /// Convert the given `Message` struct into a vector of bytes.
    fn into_bytes(self) -> GoopyResult<Vec<u8>> {
        let mut idm_bytes = Vec::new();
        idm_bytes.push(self.id);
        idm_bytes.extend_from_slice(&self.data.into_bytes());
        Ok(idm_bytes)
    }

    /// Convert a vector of bytes into a `Message` struct.
    fn from_bytes(bytes: Vec<u8>) -> GoopyResult<Message> {
        let mut iter = bytes.into_iter();

        // Get the Message Type Byte
        let id = if let Some(mb) = iter.by_ref().next() {
            mb
        } else {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        };

        if id != SSH_MSG_IGNORE {
            return Err(GoopyErr::InvalidPacketByte("message-byte"));
        }

        let data_bytes: Vec<u8> = iter.by_ref().collect();
        let data = String::from_utf8_lossy(&data_bytes).into_owned();

        Ok(Message {
            id: id,
            data: data,
        })
    }
}

#[cfg(test)]
mod idm_test {


    use libc::{c_void, memcmp};
    use message::{Payload, SSH_MSG_IGNORE};
    use std::default::Default;
    use super::Message;

    const TEST_IDM_BYTES: [u8; 25] = [2, 70, 101, 101, 108, 32, 102, 114, 101, 101, 32, 116, 111,
                                      32, 105, 103, 110, 111, 114, 101, 32, 116, 104, 105, 115];

    #[test]
    fn default_idm() {
        let idm: Message = Default::default();
        assert!(SSH_MSG_IGNORE == idm.id);
        assert!(idm.data.is_empty());
    }

    #[test]
    fn idm_into_bytes() {
        let mut idm: Message = Default::default();
        idm.set_data("Feel free to ignore this".to_string());

        if let Ok(bytes) = idm.into_bytes() {
            let len = TEST_IDM_BYTES.len();
            let t_ptr = TEST_IDM_BYTES.as_ptr() as *const c_void;
            let b_ptr = bytes.as_ptr() as *const c_void;
            unsafe {
                assert!(0 == memcmp(t_ptr, b_ptr, len));
            }
        } else {
            assert!(false);
        }
    }

    #[test]
    fn idm_from_bytes() {
        let mut test_vec = Vec::new();
        test_vec.extend_from_slice(&TEST_IDM_BYTES);
        if let Ok(idm) = Message::from_bytes(test_vec) {
            assert!(idm.id == SSH_MSG_IGNORE);
            assert!(&idm.data == "Feel free to ignore this")
        } else {
            assert!(false);
        }
    }
}
